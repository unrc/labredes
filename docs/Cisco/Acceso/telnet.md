# Telnet

## Configuración Previa 

* Dado que la conexión es por medio de red, es necesario que el conmutador cuente con una dirección IP. Su configuración es mediante: ([enlace](../Switch/Vlan/IP.md)).

* Para implementar telnet es necesario tener configurada una contraseña en el terminal de configuración ([enlace](../Contrasenas)).

* El acceso desde un cliente es mediante el comando: 

```
telnet "IPsw"
```

Donde: |
-|
**IPsw** = Dirección IP del conmutador |

## Contraseña compartida
Acceso a configuración telnet:
Carga de contraseña compartida:
Habilitar la autenticación:

```
equipo>
equipo> enable
equipo# configure terminal
equipo(config)#line vty 0 15
equipo(config-line)#password contraseña
equipo(config-line)#login 
```

Donde: | |
-- | --
**contraseña** = contraseña asignada a la conexión telnet | |

## Múltiples usuarios con sus respectivas contraseñas
Carga de la asociación usuario y contraseña:
Acceso a configuración telnet:
No se utiliza contraseña compartida:
Autenticación con usuario/contraseña:

```
equipo>
equipo> enable
equipo# configure terminal
equipo(config)#username "nombrei" secret "contraseñai"
equipo(config)#line vty 0 15
equipo(config-line)#no password
equipo(config-line)#login local
```

Donde: | |
-- | --
**nombrei**  = nombre asignada al usuario i | **contraseñai** = contraseña asignada al usuario i

## Visualización de usuarios conectados

```
equipo>
equipo> enable
equipo#show users
```
