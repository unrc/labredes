# Cuardar Configuración de equipo Cisco

## Borrado de configuración general

Aplicando el siguiente comando, es posible borrar gran parte de la configuración del equipo. Tener presente que para eliminar la configuración de vlans, se debe ejecutar el paso que se indica posteriormente.

```
equipo# write erase
```

## Limpieza de configuración de VLANs

```
equipo# delete flash:vlan.dat
```
