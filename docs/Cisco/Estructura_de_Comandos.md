# Estructura de comandos

Cisco presenta varios modos de configuración, los cuales permiten realizar diferentes acciones y/o configuraciones. Los modos más utilizados son: 


![](imagenes/modos.jpg)

## Modos de un Router

Modo de configuración inicial (SETUP) | Modo RXBOOT
-- | --
Diálogo con indicadores utilizado para establecer una configuración inicial. | Recuperación de desastres en caso de pérdida de la contraseña o si el sistema operativo se borra accidentalmente de la Flash.


Modo EXEC del usuario | Modo EXEC privilegiado |
-- | --
Análisis limitado del dispositivo. Acceso remoto. | Análisis detallado del dispositivo. Depuración y prueba. Manipulación de archivos. Acceso remoto.
equipo> | equipo# |

Modo de configuración global | Otros modos de configuración
-- | --
Comandos de configuración simple. | Configuraciones complejas y de múltiples líneas. 
equipo(config)# | equipo(config - mode)#

modo de configuración de las interfaces | modo de configuración de la líneas de ingreso
-- | --
equipo(config-if)# | equipo(config-line)# | 
