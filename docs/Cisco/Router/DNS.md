# DNS

## Configuración de cliente DNS

Para indicar un servidor DNS donde hacer consultas, implementar el siguiente comando:

```
Router> enable
Router# configure terminal
Router(config)# ip name-server "d.d.d.d"
Router(config)# ip domain lookup
```

Donde: | |
-- | --
**d.d.d.d** = dirección IP del servidor DNS | |

## Configuración de un enrutador como servidor DNS

Una vez configurado el enrutador como cliente DNS, el mismo puede aceptar peticiones. Para ello, se debe habilitar el servicio mediante el siguiente comando:

```
Router> enable
Router# configure terminal
Router(config)# ip dns server 
```