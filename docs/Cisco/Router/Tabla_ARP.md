# Tabla ARP

## Visualización de Tabla ARP

```
Router# show arp

Protocol      Address    Age (min)   Hardware Addr   Type     Interface
"protocolo"   "Dir C3"   "tiempo"    "Dir C2"        "tipo"   "Interfaz"
```

| Donde: | |
| -- | -- |
| **protocolo** = protocolo de capa 3 | **Dir C3** = Dirección Capa 3 |
| **tiempo** = tiempo desde la asociacíon dirección capa 3 con dirección capa 2 | **Dir C2** = Dirección Capa 2 |
| **tipo** = tipo de asociación dirección capa 3 con dirección capa 2 | **Interfaz** = Interfaz a la que se alcanza el dispositivo |
