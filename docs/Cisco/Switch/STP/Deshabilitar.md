# Deshabilitar STP en una vlan

IMPORTANTE: Cuando STP está deshabilitada y existen lazos en la topología de la red, puede existir excesivo tráfico, que disminuya la performance de la red.

El siguiente comando deshabilita stp en un vlan en particular:

```bash
Switch# configure terminal
Switch(config)# no spanning-tree vlan "#v"
```

Donde: | |
-- | --
**#v** = número de vlan a la que se le deshabilita stp | 
