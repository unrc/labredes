# Tabla MAC

## Visualización Tabla MAC

```
Switch# show mac address-table
Mac Address Table
-------------------------------------------------
Vlan     Mac Address           Type         Ports
----     -----------           --------     -----
"#v"     "xx:xx:xx:xx:xx:xx"   "tipo"       "interfaz"
```

Donde: | |
-- | --
**#v** = identificador de vlan | **xx:xx:xx:xx:xx:xx** = dirección MAC
**tipo** = indica si la entrada es estática o dinámica | **interfaz** = interfaz desde la que se alcanza la dirección MAC

<table>
    <th><img src="https://unrc.gitlab.io/labredes/Cisco/GNS3_logo.png"></th>
    <th><strong style="color: red;">Importante:</strong> en caso de utilizar un equipo Etherswitch Router Cisco 3725, la tabla resultante cambia su aspecto</th>
</table>
