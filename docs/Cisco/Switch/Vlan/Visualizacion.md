## Visualización puertos en modo acceso

```
Switch# show vlan
VLAN     Name       Status     Ports
----     ------     ------     -------------------------------
"#v"     "nombre"   "estado"   "puertos"
```

Donde: | |
-- | --
**#v** = número de vlan | **nombre** = nombre de la vlan 
**estado** = activo/inactivo | **puertos** = interfaces asociadas a cada vlan


<table>
    <th><img src="https://unrc.gitlab.io/labredes/Cisco/GNS3_logo.png"></th>
    <th><strong style="color: red;">Importante:</strong> en caso de utilizar un equipo Etherswitch Router Cisco 3725, el comando es: <strong>show vlan-switch</strong></th>
</table>

## Visualización puertos en modo troncal

```
Switch# show interfaces trunk 
Port        Mode         Encapsulation  Status        Native vlan
"puertos"   on           "protocolo"    trunking      1
```

Donde: | |
-- | --
**puertos** = interfaces asociadas a cada vlan | **protocolo** = protocolo de encapsulación |
