# Entorno de Trabajo

El entorno de trabajo cuenta con las siguientes secciones:

![](Entorno_de_trabajo.png)
<p style="text-align: center;">Figura 1 - Entorno de trabajo</p>

## Ejemplo
Por ejemplo si se desea ingresar un enrutador en un diagrama, se debe buscar el elemento en la **Sección de elementos** y lego de escoger el más indicado, presionar sobre la imagen y arrastrarla hacia el **Área de trabajo**. Las propiedades del elemento se pueden modificar en la **Sección de edición de propiedades elementos**:

![](Ejemplo_Router.png)
<p style="text-align: center;">Figura 2 - Ejemplo</p>