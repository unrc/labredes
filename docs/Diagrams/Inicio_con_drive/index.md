#Inicio con cuenta de google drive

## Acceso al sitio web
Ingresar al sitio [diagrams.net](diagrams.net)

![](diagrams_inicial.png)
<p style="text-align: center;">Figura 1 - Acceso a diagrams.net</p>

Una vez se termine de cargar como se muestra en la figura 1, seleccionar la opción **Start Now** o **Start**

Si se utiliza por primera vez, se indicará las opciones de almacenamiento disponibles.

## Opciones de guardado

Se recomienda que se guarden los diagramas en su cuenta de **Google Drive**, pero también se puede utilizar otro tipo de plataformas, descargarlo en la PC (en la opción **Device**) o decidirlo luego (en la opción **Decide later**)

![](Guardado.png)
<p style="text-align: center;">Figura 2 - Opciones de guardado</p>

### Guardado en Google Drive

En caso de seleccionar **Google Drive**, la plataforma solicitará autorización para conextarse con la cuenta y archivos de Drive

![](Autorizacion.png)
<p style="text-align: center;">Figura 3 - Solicitud de autorización</p>

Es posible que si el navegador tiene desactivadas las ventanas emergentes, solicite permisos. En caso de estar utilizando Firefox, se presentará algo como lo que se muestra en la siguiente figura:

![](Error_POP.png)
<p style="text-align: center;">Figura 4 - Error Pestañas Emergentes</p>

Para solucionarlo, presionar en **Preferences**/**Preferencias** y habilitar las ventanas emergentes para diagrams.net

Una vez se abra la ventana emergente, acceder a su cuenta de google y dar permisos para que diagrams.net pueda acceder y editar sus archivos:

![](Login.png)
<p style="text-align: center;">Figura 5 - Acceso a cuenta de Google</p>

**Importante**: puede que en algunos casos se deban repetir estos pasos por problemas entre el navegador y las cookies del sitio web.