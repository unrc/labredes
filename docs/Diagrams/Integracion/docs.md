#Integración con Google Docs

Una vez dentro de un documento de google, seleccionar en la barra superior (como se muestra en la figura 1) las opciones:
**Extensiones > Complementos > Descargar Complementos**

![](instalacion_1.png)

<p style="text-align: center;">Figura 1 - Descarga de Complementos</p>


Dentro de la opción de **Descarga de Complementos**, ingresar el nombre que se desea instalar **Diagrams for docs**:

![](instalacion_2.png)

<p style="text-align: center;">Figura 2 - Búsqueda del complemento</p>

Finalmente, dentro de los resultados de búsqueda, seleccionar **Diagams for docs**:

![](instalacion_3.png)
<p style="text-align: center;">Figura 3 - Instalación</p>