# Comandos Principales

## Iniciar/ejecutar el proyecto

Crear y ejecuta los contenedores.

```bash
docker-compose up
```

## Eliminar los contenedores del proyecto

```bash
docker-compose down
```

## Visualizar el estado de los contenedores del proyecto

```bash
docker-compose ps
```

## Ejecutar comandos en contenedores

Permite ejecutar un comando a uno de los servicios levantados de Docker-compose

```bash
docker-compose exec
```
