# Docker Compose - Archivo de configuración

## Definir dependencias.

### Crear una carpeta para el proyecto:

```bash
mkdir "NombreCarpeta"
cd "NombreCarpeta"
```

Donde: ||
-- | -- |
**NombreCarpeta** = Nombre de la carpeta a donde se va a alojar el proyecto | |

### Archivo **app.py**

Dentro de la carpeta, crear un archivo llamado **app.py**, cuyo contenido debe ser:

```bash
import time

import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='"Nombre"', port="Puerto")

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)
```

Donde: ||
-- | -- |
**Nombre** = Nombre de host asignado al contenedor dentro de la red | |
**Puerto** = Puerto desde el cual va a ser alcanzado| |

### Archivo **requirements.txt**

Crear otro archivo llamado **requirements.txt**, con el contenido:

```bash
flask
redis
```

## Archivo **Dockerfile**

Crear un nuevo archivo llamado **Dockerfile** que contenga lo siguiente:

```bash
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask", "run"]
```

## Archivo **docker-compose.yml**

Finalmente, crear un archivo llamado **docker-compose.yml** con:

```bash
version: "3.8"
services:
  web:
    build: .
    ports:
      - "5000:5000"
  redis:
    image: "redis:alpine"
```

## Construir y ejecutar la configuración

Dentro de la carpeta del proyecto ejecutar el siguiente comando:

```bash
docker-compose up
```
