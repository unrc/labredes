# Macvlan

Este driver permite aislar el stak de protocols entre el contenedor y la PC anfitrión, por lo que suele ser la mejor opción en implementaciones que esperan estar conectadas directamente a la red física, en lugar de enrutarse a través de la pila de red de la PC anfitrión.

Más información sobre Macvlan: [link](https://docs.docker.com/network/macvlan/)

Más información de la configuración dentro de un contenedor: [link](https://docs.docker.com/network/network-tutorial-macvlan/)

## Carga de una red macvlan

Crear una macvlan asociada a una interfaz en particular:

```bash
docker network create --driver macvlan \
  --subnet="IPRed"/"Mascara" \
  --ip-range="IPRango"/"MascaraRango" \
  --gateway="GW" \
  -o parent="Interfaz" "NombreRed"
```

Donde: ||
-- | -- |
**IPRed** = Dirección IP de red | **Mascara** = Máscara de red |
**GW** = Puerta de enlace de la red | **Interfaz** = Interfaz del host |
**NombreRed** = Nombre asociado a la nueva red | |

Notar:

* "--driver macvlan" especifica que se lleva a cabo este tipo de configuración.

* "--ip-range " determina un pool de direcciones a partir de una subred ("IPRango"/"MascaraRango") para asignar a los contenedores.

## Ejemplo de asignación de red "macvlan" con comando docker

```bash
docker run -it --network "NombreRed" ubuntu:latest bash
```

Donde: ||
-- | -- |
**NombreRed** = Nombre asociado a la red macvlan creada con anterioridad | |
