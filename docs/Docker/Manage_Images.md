# Magane Docker images

## Add a image in linux:
```bash
docker pull "image_name":"tag"
```

Example
```bash
docker pull ubuntu:18.04
```

## View all images:
```bash
docker images
```

## Remove a image:
```bash
docker rmi "docker_id"
```