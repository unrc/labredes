## Crear una nueva imagen a partir de un contenedor

```bash
docker commit "IdContenedor" "NombreImagen":"Etiqueta"
```

Donde: ||
--  | -- |
**IdContenedor** = Número de identificación del contenedor | **NombreImagen** = Nombre asociado a la imagen |
**Etiqueta** = Etiqueta para el versionado de la imagen | |

## Compartir una imagen 
### Exportar una imagen en un archivo .tar

```bash
docker export "IDContenedor" > "NombreArchivo".tar
```

Donde: ||
--  | -- |
**IdContenedor** = Número de identificación del contenedor | **NombreArchivo** = Usuario de la plataforma docker |

### Import an image from a .tar file

```bash
docker import "NombreArchivo".tar "NombreImagen":"Etiqueta"
```

Donde: ||
--  | -- |
**NombreArchivo** = Usuario de la plataforma docker | **NombreImagen** = Nombre asociado a la imagen |
**Etiqueta** = Etiqueta para el versionado de la imagen | |

## Cargar una imagen en docker hub

Generar un contenedor con la siguiente etiqueta: "NombreUsuario"/"NombreImagen":"Etiqueta"

```bash
docker push "NombreUsuario"/"NombreImagen":"Etiqueta"
```

Donde: ||
--  | -- |
**NombreUsuario** = Usuario de la plataforma docker | **NombreImagen** = Nombre asociado a la imagen |
**Etiqueta** = Etiqueta para el versionado de la imagen | |