#Contenedores

Al seleccionar la opción containers del menú de la izquierda, se accede al listado de contenedores existentes en la PC anfitrión y a la posibilidad de crear nuevos contenedores, como se muestra en la figura 1:

![](imagenes/containers_list.png)
Figura 1. Listado de contenedores

##Carga de un nuevo contenedor

Al presionar el ícono azul **Add container** presente en la parte superior derecha de la pantalla (ver figura 1), se accede a la sección donde se puede generar un nuevo contenedor. Considerando la figura 2, inicialmente se debe:

* ingresar un nombre al contenedor.
* especificar la imagen desde la cual se creará.
* habilitar los puertos:
    * aquellos que se encuentren pre configurados en la imagen de referencia.
    * indicados manualmente.
* configurar control de acceso.

![](imagenes/create_container.png)
Figura 2. Listado de contenedores

Posterior a ello, como se muestra en la figura 3, se pueden configurar opciones avanzadas, como:

* comandos a ejecutar.
* opciones de red.
* volúmenes.

![](imagenes/create_container2.png)
Figura 3. Listado de contenedores