#Imagenes

Al seleccionar la opción **Images** del menú de la izquierda, se accede a una sección de descarga de imágenes de contenedores y al listado de las imágenes disponibles en la PC anfitrión:

![](imagenes/images_list.png)

