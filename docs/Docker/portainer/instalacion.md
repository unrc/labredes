#Introducción/Instalación

Portainer puede utilizarse directamente desde un contenedor. El mismo se compone de dos elementos: **Portainer Server** y **Portainer Agent**

Tutorial de referencia: [https://docs.portainer.io/start/install-ce/server/docker/linux](https://docs.portainer.io/start/install-ce/server/docker/linux)

## Instalación

Para la descarga y configuración del contenedor, es necesario ejecutar lo siguiente:

<!-- language: lang-bash -->
```bash
$ docker volume create portainer_data

$ docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest
```

El primer comando crea un volumen, el cual se utilizará para almacenar una base de datos.

El segundo, permite la creación (y en caso que sea necesaria la descarga de la imagen) del contenedor en cuestión. 
Dentro de los argumentos se puede apreciar que se mapean los puertos 8000 y 9443, se asigna el nombre "portainer".

## Acceso/Configuración Inicial

Ingresar al entorno web de portainer desde la PC anfitrión, por medio de la siguiente url: [https://localhost:9443](https://localhost:9443)

### Carga de usuario administrador

Durante la primera carga del entorno web, será necesaria la creación de un nombre y contraseña (con longitud mínima de 12 caracteres) que se corresponde con usuario el administrador: 

![](imagenes/user.png)

### Configuración inicial

Una vez creado el usuario administrador, se accederá al asistente de configuración, el cuál automáticamente detectará el entorno local:

![](imagenes/wizard.png)