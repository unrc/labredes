#Networks

La opción **Networks** del menú de la izquierda, presenta un listado de las opciones de configuración de red:

![](imagenes/networks.png)
Figura 1. Listado de redes

##Detalle de red
Al seleccionada una red en particular, se puede observar su configuración completa:

![](imagenes/network_detail1.png)
Figura 2. Detalle de red

En la figura 2 se presenta la parte inicial del detalle, mientras que en la siguiente imagen se observa la descripción del control de acceso, opciones de red y el listado de contenedores conectados a dicha red:

![](imagenes/network_detail1.png)
Figura 3. Detalle de red, segunda parte

##Carga de una nueva red

En caso que sea necesario crear una nueva red, se puede adicionar a partir del botón azul, presente en la parte superior derecha, como se muestra en la figura 1. Una vez en la sección, se debe ingresar:

* ingresar nombre.
* seleccionar el tipo de red (en función a los drivers presentes).
* carga de configuración IPv4.
* carga de parámetros IPv6.
* indicar si la red debe estar aislada o no.
* permitir que se puedan unir o no contenedores en forma manual.
* carga de permisos de acceso.

Tener presente que algunos de estos parámetros pueden variar en función al tipo de red que se ha de adicionar.

![](imagenes/create_network.png)
Figura 4. Carga de una nueva red