#Acceso Web

Como se observa a continuación, el funcionamiento de GNS3 funciona bajo un esquema **servidor-cliente**:

![](imagenes/GNS3-esquema.png)

Los modos de acceso son a partir de gns3-gui (software gráfico) o con un navegador web. Estos modos pueden accederse en forma local o remota.

##Modo de acceso

Para acceder, es necesario abrir un navegador web y como dirección ingresar lo siguiente: **http://IPservidor:3080**

![](imagenes/WebClient.png)

El puerto por defecto es el **3080**, el mismo puede cambiarse en el archivo **~/.config/GNS3/2.2/gns3_server.conf**.

Tener presente que este modo de uso se encuentra en desarrollo, por lo que aún no es posible la visualización de capturas con Wireshark.

##Cambio de nombre de usuario y contraseña

Para modificar el nombre de usuario  y contraseña, es necesario editar el archivo: **~/.config/GNS3/2.2/gns3_server.conf**

```bash
[Server]
path = /usr/bin/gns3server
ubridge_path = /usr/bin/ubridge
host = 0.0.0.0
port = 3080
images_path = /home/compu/GNS3/images
projects_path = /home/compu/GNS3/projects
appliances_path = /home/compu/GNS3/appliances
additional_images_paths = 
symbols_path = /home/compu/GNS3/symbols
configs_path = /home/compu/GNS3/configs
report_errors = True
auto_start = True
allow_console_from_anywhere = False
auth = True
user = "Usuario"
password = "Contraseña"
protocol = http
console_start_port_range = 5000
console_end_port_range = 10000
udp_start_port_range = 10000
udp_end_port_range = 20000
```

Donde: | |
-- | -- |
**Usuario**= Nombre de usuario (por defecto admin) | **Contraseña**= Contraseña
