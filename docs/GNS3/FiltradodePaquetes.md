#Filtrado de paquetes

Mediante esta herramienta, es posible modificar el comportamiento de un elace.

![](imagenes/Filtro.png)

Importante: Estos filtros son aplicados al tráfico en ambos sentidos.

Para ello, se debe hacer click con el botón derecho sobre el enlace virtual y seleccionar la opción "Packet Filter".

Dentro de las posibilidades, se encuentan las siguientes variaciones:

* Frequency drop: representa una frecuencia de pérdida de paquetes, que se configura con:
			-1 -> eliminar todos los paquetes.
            número positivo ->  se elimina uno de cada determinados paquetes.
            0 -> no se eliminan paquetes.

* Packet loss: Porcentaje que representa la probabilidad de pérdida de paquetes.

* Delay: Retardo que se le aplica a todos los paquetes [ms]. A su vez es posible adicionar jitter [ms] (+/-).

* Corrupt: Porcentaje que representa la probabilidad de que un paquete se dañe.

* Berkeley Packet Filter (BPF): Este filtro elimina cualquier paquete que cumpla con la expresión BPF.