# Conexión a interfaces del anfitrión

Para conectar el entorno de gns3 con interfaces físicas o virtuales del anfitrión, se puede utilizar el elemento **cloud**.

## Interfaces Virtualex (Ejemplo Virbr0)

Buscar en el menú de la izquierda la opción donde se listan todos los elementos y seleccionar "cloud":

![](imagenes/MenuVirbr0.png)

Dentro de la configuración de cloud, habilitar "Mostrar interfaces ethernet especiales", seleccionar la interfaz virtual (en el ejemplo se muestra virbr0) y adicionarla: 

![](imagenes/NodoVirbr0.png)
