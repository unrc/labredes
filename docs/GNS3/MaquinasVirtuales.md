#Integración de Máquinas Virtuales a GNS3

Para integrar una máquina virtual de VirtualBox en GNS3, seleccionar la opción "VirtualBox VMs" dentro del menú "Editar > Preferencias", como se muestra en la figura:

![](imagenes/vm.png)