# Acceso a internet

Al momento de realizar la instalación de GNS3 en una PC, se realiza la instalación de la librería [libvirt](https://libvirt.org/). Esta librería cuenta con herramientas para la virtualización de interfaces y redes. Con ella, se adiciona en forma automática la interfaz virtual **virbr0** al ordenador, la cual tiene por defecto los siguientes parámetros:

* Dirección IP: 192.168.122.1/24
* Reenvío de paquetes habilitado entre la red 192.168.122.0/24 y el resto de redes a las que está conectada la computadora, con lo cual permite el enrutamiento de paquetes.
* Implementa NAT, ocultando **sólo** la red 192.168.122.0/24.
* Servidor de DHCP.

## Bloque de acceso a internet

El bloque **NAT** es un nodo que permite interactuar directamente con la interfaz virtual virbr0, por lo que al integrarlo a una red GNS3, éste nodo ofrece acceso a internet y configuración IP dentro de la red 192.168.122.0/24 en forma automática.

![](NAT.jpeg)
Figura 1 - Nodo NAT
