# Modos de Acceso

## Uso de la consola

Inicio del dispositivo

Modo | Descripción
-- | --
\<Huawei\> | "user-view": modo usuario, observar pero no realizar configuraciones.
\[Huawei\] | "system-view": modo privilegiado, permite acceder a las configuraciones.
