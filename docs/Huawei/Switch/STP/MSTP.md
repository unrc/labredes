# Múltiple Spanning Tree

## Configuración de una región

Creación de una región y asignación de vlans a instancias:

```bash
[Huawei] stp region-configuration
[Huawei-mst-region] region-name "nombre"
[Huawei-mst-region] instance "#i" vlan "#v" to "#v"
[Huawei-mst-region] active region-configuration
[Huawei-mst-region] quit
```

Donde: ||
-- | -- |
**nombre** = nombre de la región | **#i** = número de instancia |
**#v** = número de vlan | |

## Cambio de prioridad a instancias

### Prioridad primary/secondary

```bash
[Huawei] stp instance "#i" root "primary/secondary"
```

Donde: ||
-- | -- |
**#i** = número de instancia | **primary/secondary** = asigna prioridad primaria o secundaria |

### Prioridad manual

```bash
[Huawei] stp instance "#i" priority "prioridad"
```

Donde: ||
-- | -- |
**#i** = número de instancia | **prioridad** = primary/secondary/valor prioridad asignada al switch para esa instancia |
