# Crear vlan

## Agregar vlan

```bash
<Huawei>system-view
[Huawei]vlan "#v"
[Huawei-vlan#]description "descripción"
[Huawei-vlan#]quit
```

Donde: | |
-- | -- |
**#v** = número de la vlan | **descripción** = descripción asociada a la vlan|
