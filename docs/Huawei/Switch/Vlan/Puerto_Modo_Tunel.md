# Configuración modo Tunel

```bash
<Huawei>system-view
[Huawei]interface "tipo" "#i"
[Huawei-interfaz#i]port link-type dot1q-tunnel
[Huawei-interfaz#i]quit
```

Donde: ||
-- | -- |
**tipo** = tipo de interfaz | **#i** = identificador de la interfaz
