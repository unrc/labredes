El siguiente proyecto GNS3 permite analizar el funcionamiento de un servidor DHCP en equipamiento Mikrotik y clientes en Linux y VPCs.

Figura 1 - Diagrama de red
Elementos:

Nombre | Descripción |
--|--|
LabRedes | Enrutador con configuración equivalente a la del laboratorio de redes |
S-DHCP | Servidor DHCP implementado en un enrutador Mikrotik |
vpc | Máquina virtualizada del tipo VPCS |
dhcp-alpine	| Máquina con Linux Alpine que cuenta con el comando "dhclient" |

Descarga del proyecto: enlace

Configuración de LabRedes
El lado derecho del proyecto, incluyendo el equipo LabRedes se corresponde con la implementación ofrecida en la implementación de LabRedes en GNS3.

Configuración de S-DHCP
A continuación se muestra el script con el listado de comandos que hacen al dispositivo Mikrotik que cuenta con el servidor DHCP:

´´´bash
# Configuracion de nombre de equipo:
/system identity set name=S-DHCP-Mikrotik

# Asignacion de direcciones IP a interfaces
/ip address add address=192.168.1.100/24 interface=ether1 network=192.168.1.0
/ip address add address=192.168.100.1/24 interface=ether2 network=192.168.100.0

# Asignacion de puerta de enlace
/ip route add dst-address=0.0.0.0/0 gateway=192.168.1.1

# Configuracion de servidor DNS
/ip dns set allow-remote-requests=yes servers=192.168.1.1,200.7.141.6,200.7.141.7

# Configuracion de NAT
/ip firewall nat add action=masquerade chain=srcnat

# Reserva de pool de direcciones
/ip pool add name=Pool-DHCP ranges=192.168.100.100-192.168.100.199

# Configuracion de servidor DHCP
/ip dhcp-server network add address=192.168.100.0/24 dns-server=192.168.100.1 gateway=192.168.100.1
/ip dhcp-server add address-pool=Pool-DHCP disabled=no interface=ether2 name=ServidorDHCP
´´´

##Uso de máquina vpc
Para conocer los comandos de este tipo de máquinas virtualizadas, visitar el siguiente enlace

##Uso de máquina dhcp-alpine
Al igual que en distribuciones de Ubuntu, para obtener configuración por DHCP, ejecutar el siguiente comando:

´´´bash
dhclient
´´´

En el caso de esta implementación no es necesario anteponer sudo ya que ya el usuario se encuentra con permisos de superusuario.

Para conocer diferencias en comandos de Linux Alpine y Ubuntu, visitar el siguiente enlace