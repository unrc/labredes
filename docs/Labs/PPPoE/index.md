# PPPoE

El siguiente laboratorio presenta la implementación de un servidor PPPoE configurado en un equipo Mikrotik y dos clientes PPPoE implementados en dos Linux.

## Prerequisitos

Para la implementación de este laboratorio se requieren los siguientes programas:

Docker [enlace de instalación](https://unrc.gitlab.io/labredes/docker/Docker_Instalacion/)

GNS3 [enlace de instalación](https://unrc.gitlab.io/labredes/gns3/Instalacion/)

## Diagrama de red

[](docs/Labs/PPPoE/PPPoE.png)

## Servidor PPPoE Mikrotik

Tutorial de configuración: [enlace](https://unrc.gitlab.io/labredes/mikrotik/PPP/servidor/)

Pasos de configuración:



## Cliente PPPoE Linux

Tutorial de configuración: [enlace](https://unrc.gitlab.io/labredes/linux/PPP/Cliente/)

