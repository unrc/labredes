# Netplan

**Netplan** es un gestor de redes que se encuentra en Ubuntu a partir de la versión 18.04. El archivo de configuración es: /etc/netplan/01-netcfg.yaml (o el nombre que se encuentre en esa carpeta). En este archivo se puede especificar los siguientes aspectos:

* Asignar una dirección de IP a las placas de red de la PC (estática o cliente DHCP)
* Agregar rutas
* Definir servidores DNS

Sus ventajas son:

* Se configura todo desde un único archivo
* Fácil de editar

### Configuración de dirección IP ESTÁTICA + RUTAS + DNS a una interfaz
```bash
 ethernets:
    "interfaz":
      dhcp4: "yes/no"
      addresses: ["x.x.x.x"/"y"]
      routes:
        - to: default
          via: "g.g.g.g"
     nameservers:
       addresses: ["d.d.d.d"]
```

Donde:||
-- | -- |
**interfaz** = nombre de la interfaz a configurar | **yes/no** = permite o no la implementación de cliente DHCP |
**x.x.x.x** = dirección IP estática | **y** = máscara de red en formato decimal |
**g.g.g.g** = dirección IP de la puerta de enlace | **d.d.d.d** = dirección IP del servidor DNS |

### Configuración de una interfaz como cliente DHCP
```bash
ethernets:
    "interfaz":
     dhcp4: "yes/no"
```

Donde:||
-- | -- |
**interfaz** = nombre de la interfaz a configurar como cliente DHCP | **yes/no** = permite o no la implementación de cliente DHCP

### Aplicación de los cambios
Después de editar el archivo es necesario reiniciar los servicios de red para que se ejecuten los cambios:
```bash
sudo netplan apply
```

### Debug
En caso de tener inconvenientes en la configuración, puede accederse al archivo de debug con el siguiente comando:
```bash
sudo netplan --debug apply
```

