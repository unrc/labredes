# DNS
Para configurar hay que editar el archivo /etc/resolv.conf y por cada servidor, ingresar una línea con la estructura “nameserver IPservidor”. 

Ejemplo con comando **echo**:

```bash
sudo su
echo nameserver "d.d.d.d" > /etc/resolv.conf
```

Donde: | |
-- | -- 
**d.d.d.d** = dirección IP del servidor DNS
