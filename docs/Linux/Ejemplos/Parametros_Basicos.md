# Ejemplo 1 - Configuración Básica

Considerando el siguiente esquema:

![](../imagenes/Diagrama1.png)

Configurar la PC Linux para que tenga acceso a internet.

## 1.1 Dirección IP

### 1.1.1 Comando ip

Tutorial de referencia: [enlace](/linux/Direcciones_IP/ip_address)

<iframe width="728" height="410" src="https://www.youtube.com/embed/Ur6c5FAwqmY" title="Configuración Dirección IP - Comando ip" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### 1.1.2 Comando ifconfig

Tutorial de referencia: [enlace](/linux/Direcciones_IP/ifconfig)

<iframe width="728" height="410" src="https://www.youtube.com/embed/JOVyE3_jnoE" title="Configuración Dirección IP - Comando ifconfig" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 1.2 Puerta de enlace

### 1.2.1 Comando ip

Tutorial de referencia: [enlace](/linux/Tabla_de_rutas/ip_route)

<iframe width="728" height="410" src="https://www.youtube.com/embed/44P8jSCDM9k" title="Configuración Puerta de enlace - comando ip" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### 1.2.2 Comando route

Tutorial de referencia: [enlace](/linux/Tabla_de_rutas/route)

<iframe width="728" height="410" src="https://www.youtube.com/embed/PKnk6HxNn0I" title="Configuración Puerta de enlace - comando route" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 1.3 DNS

Tutorial de referencia: [enlace](/linux/DNS)

<iframe width="728" height="410" src="https://www.youtube.com/embed/J2Ss9y8-fe8" title="Configuración DNS" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
