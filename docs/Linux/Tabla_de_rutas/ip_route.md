# Tabla de rutas - Comando ip route

**ip route** se encuentra disponible en versiones recientes de Linux.

De ser necesaria su instalación, adicionar el paquete **iproute2** como se muestra en: [enlace](/linux/Direcciones_IP/ip_address). 

De no encontrarse en el sistema, puede utilizarse el [comando route](/linux/Tabla_de_rutas/route).

## Visualización de puerta de enlace/tabla de rutas

```bash
ip -c -br route
```

El argumento **-c** permite resaltar parte de la configuración con colores, mientras que **-br** organiza el resultado en forma de tabla. 

## Configuración de puerta de enlace (Gateway)

```bash
sudo ip route "add/del" default via "g.g.g.g"
```

Donde: | |
-- | -- 
**add** = adiciona una nueva línea a la tabla de rutas | **del** = elimina una línea preexistente en la tabla de rutas
**g.g.g.g** = dirección IP de la puerta de enlace

### Ejemplo

Para a un caso práctico, visualizar el video [ejemplo 1.2.1](/linux/Ejemplos/Parametros_Basicos/#121-comando-ip)

## Carga de rutas en forma estática

```bash
sudo ip route "add/del" "x.x.x.x"/"y" via "g.g.g.g" dev "interfaz"
```

Donde: | |
-- | --
**add** = adiciona una nueva línea a la tabla | **del** = elimina una línea preexistente en la tabla
**x.x.x.x** = dirección de red a alcanzar | **y** o **y.y.y.y** = máscara de red en decimal u octal
**g.g.g.g** = dirección IP del próximo salto | **interfaz** = interfaz que conecta al próximo salto
