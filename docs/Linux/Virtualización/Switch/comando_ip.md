# Configuración de Bridge mediante comando ip

Linux permite por medio del comando **ip** la creación de bridges (switches virtuales) y la asignación de interfaces, ya sean físicas como lógicas.

## Creación de un Bridge y asignación de interfaces

### Creación

```
ip link add name "NombreBridge" type bridge
ip link set dev "NombreBridge" up
```

Donde: |
--|
"NombreBridge" = Nombre del bridge a crear |

### Asignación de interfaces a un bridge


```
ip link set dev "NombreInterfaz" master "NombreBridge"
```

Donde: | |
--| --
**NombreInterfaz** = nombre de interfaz a asignar | **NombreBridge** = Nombre del bridge al que se le asigna la interfaz |

## Ejemplo de análisis

Para crear un bridge como se muestra en la PC del centro de la siguiente figura:

![](topology.jpg)

se deben ejecutar los siguientes comandos:

```
labredes@PC2# ip link add name br0 type bridge
labredes@PC2# ip link set dev br0 up
labredes@PC2# link set dev eth0 master br0
labredes@PC2# link set dev eth1 master br0
```
