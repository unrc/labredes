# Forward - modos para el reenvío de paquetes

La interfaz virtual puede implementar múltiples comportamientos para el reenvío de paquetes, en función al modo que se indique en el parámetro **forward**. 

```xml
  <forward mode='modo'/>
```

Algunas de las opciones son las siguientes:

## nat

* El tráfico generado en la red huesped puede ser reenviada al resto de las redes de la PC anfitrión.

* Implementa el reenvío de paquetes entre redes, modificando a "1" el contenido del archivo: "/proc/sys/net/ipv4/ip_forward", como se explica en el siguiente enlace: <a href="https://labredes.gitlab.io/linux/Reenvio_de_paquetes/" target="_blank">enlace</a>.

* Implementa NAT, enmascarando **la dirección de origen de aquellos paquetes pertenecientes a la red IP de virbr0** por la dirección IP de la interfaz de salida. Esto implica que si existen otras redes por debajo de la que se conecta a la virbr0, éstas no serán enmascaradas.

## route

* El tráfico generado en la red huesped puede ser reenviada al resto de las redes de la PC anfitrión.

* Implementa el reenvío de paquetes entre redes, modificando a "1" el contenido del archivo: "/proc/sys/net/ipv4/ip_forward", como se explica en el siguiente enlace: <a href="https://labredes.gitlab.io/linux/Reenvio_de_paquetes/" target="_blank">enlace</a>.

* No implementa ninguna regla de NAT.

## open

* Al igual que en el modo route, el tráfico de la red huésped puede comunicarse con la PC anfitrión.

* No modifica el reenvío de paquetes en la PC anfitrión. Para corroborar si está habilitado o implementarlo, revisar el <a href="https://labredes.gitlab.io/linux/Reenvio_de_paquetes/" target="_blank">enlace</a>.

* No implementa ninguna regla de NAT.
