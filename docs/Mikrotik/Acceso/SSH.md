## telnet

### terminal de Linux

```
telnet "IPmk"
```

Donde: |
 :-: |
**IPmk** = Dirección IP del equipo Mikrotik |

### putty

![](imagenes/telnet.jpg)

## SSH

### terminal de Linux

Para utilizar ssh se requiere el software correspondiente desde la pc que se quiere acceder. En caso de no tenerlo, se puede utilizar openssh [link instalación](https://labredes.gitlab.io/linux/Configuracion_de_red/SSH/).

```
ssh "usuario"@"IPmk"
```

Donde: ||
| :-: | :-: |
**usuario** = nombre de usuario | **IPmk** = Dirección IP del equipo Mikrotik |

### putty

![](imagenes/ssh.jpg)
