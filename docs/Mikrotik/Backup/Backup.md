


```bash
## Configuracion de MKT:
/export file=configMKT;
/system backup save name=configMKT;


## Envio de e-mail:
/tool e-mail send to="correo" subject=(" Backup - " .[/system clock get date]) body=("Backup de configuracion de Labredes - ".[/system clock get date]) file=configMKT.rsc,configMKT.backup;

/log info "Backup e-mail enviado.";
```