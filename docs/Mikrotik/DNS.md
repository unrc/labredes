# DNS

En esta sección se presenta la configuración del servicio de DNS. 

Para más información, visitar en tutorial oficial: [https://wiki.mikrotik.com/wiki/Manual:IP/DNS](https://wiki.mikrotik.com/wiki/Manual:IP/DNS)

## Configuración de cliente DNS

Para indicar un servidor DNS donde hacer consultas, implementar el siguiente comando:

```bash
/ip dns set servers="d.d.d.d"
```

Donde: | |
-- | -- |
**d.d.d.d** = dirección IP del servidor DNS | |

## Configuración de un enrutador como servidor DNS

Una vez configurado el enrutador como cliente DNS, el mismo puede aceptar peticiones. Dichas solicitudes las responderá con la información que tenga almacenada en su cache o por medio de consultas a servidores externos. Para ello, es necesario configurar el siguiente parámetro:

```bash
/ip dns set allow-remote-requests="yes/no"
```

Donde: | |
-- | -- |
**yes** = habilita las respuestas | **no** = descarta las consultas DNS que se le realicen |

### Visualización de cache

Para conocer el listados de dominios almacenados en el cache del enrutador, se debe ejecutar el siguiente comando:

```bash
/ip dns cache print 
```
Resultado:

```bash
#   NAME          TYPE      DATA    TTL  
"#" "dominio"     "tipo"    "ip"    "ttl"
```

Donde: | |
-- | -- |
**#** = número de línea | **dominio** = dominio asociado al dns |
**ip** = dirección IP asociado al dominio | |

### Limpieza de cache
A partir del comando que se presenta a continuación se libera el cache DNS:

```bash
/ip dns cache flush
```

## Visualización de configuración DNS

Para conocer la configuración DNS, implementar el siguiente comando:

```bash
/ip dns print
```
