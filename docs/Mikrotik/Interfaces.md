#Visualización de estado de interfaces

Con el comando **print** dentro de la sección **interface** se obtiene el estado de cada uno de los puertos del equipo:


```
interface/print 

Flags: R - RUNNING; S - SLAVE

Columns: NAME, TYPE, ACTUAL-MTU, L2MTU, MAX-L2MTU, MAC-ADDRESS

 #    NAME                     TYPE      ACTUAL-MTU  L2MTU  MAX-L2MTU  MAC-ADDRESS      

 0 R  "Interfaz"              "Tipo"        "MTU"                     "DireccionMAC"
 ```

 | Donde: | |
| -- | -- |
| **Interface** = nombre de la interfaz | **Tipo** = tecnología asociada a la interfaz |
| **MTU** = Valor de MTU | **DireccionMAC** = dirección MAC asociada a la interfaz |