# Captura de paquetes

RouterOS cuenta con una herramienta de captura de tráfico, la cual permite visualizar, guardar o reenviar los paquetes que pasen por una interfaz,

## Implementación por comandos

### Inicio/interrupción

Inicio de la captura:

```
/tool/sniffer/start
```

Interrupción de captura:

```
/tool/sniffer/stop
```

### Configuración de captura

A partir del comando que se presenta a continuación, es posible indicar múltiples parámetros de captura:

```
/tool/sniffer/quick "Argumentos" 
```

Donde: |
-- |
**Argumentos** = Seleccionar los argumentos del comando, seguido del valor asociado  |

Para conocer los múltiples argumentos, se puede hacer doble tab, de forma tal que Mikrotik liste las opciones posibles.

### Streaming - reenvío de tráfico

En caso de querer reenviar el tráfico a una PC que cuente con wireshark y analizarlo en tiempo real, sólo es necesario implicar el siguiente comando:

```
/tool/sniffer/set streaming-enabled="Habilitar" streaming-server="IPserver"
```

Donde: ||
-- | -- |
**Habilitar** = yes (habilita) / no (deshabilita) | **IPserver**= Dirección IP de PC donde se analiza el tráfico |

Tener presente que debe haber conectividad en L3 entre el equipo Mikrotik y la PC destino.

### Almacenamiento de captura
Para almacenar la captura, sólo es necesario indicar la opción **save** indicando el nombre del archivo, de la forma:

```
/tool/sniffer> save file-name="NombreArchivo"
```

Donde: |
-- |
**NombreArchivo** = Indica el nombre del archivo en que se almacena la captura |

## Modo Gráfico

La herramienta de captura es accesible por winbox como se muestra en la siguiente imagen:

![](imagenes/SnifferMenu.png)

### Inicio/interrupción

**Start** -> inicia la captura

** Stop** -> finaliza la captura

En el margen inferior izquierdo se muestra el estado stopped/running.

### Configuración General

La herramienta de captura de datos se encuentra en la sección: **Tools > Packet Sniffer**. Para poner en funcionamiento el programa, es necesario **aplicar** los cambios en la configuración:

* limitar el consumo de memoria
* limitar el tamaño de cada trama/paquete al conjunto de encabezados
* sobre escribir el archivo al momento que llegue al tamaño máximo
* asignar un nombre al archivo de salida
* limitar tamaño máximo del archivo resultante (en kb)

![](imagenes/SnifferGeneral.png)

una vez configurados todos los parámetros, es necesario aplicar los cambios para luego:



**Importante:** cerrar el programa no corta la captura, por lo que es necesario presionar el boton "Stop" para finalizar su funcionamiento

### Streaming - reenvío de tráfico

En caso de querer reenviar el tráfico a una PC que cuente con wireshark y analizarlo en tiempo real, sólo es necesario indicar la dirección IP de la PC destino en la pestaña **Streaming**. Tener presente que debe haber conectividad en L3 entre el equipo Mikrotik y la PC destino:

![](imagenes/SnifferStreaming.png)

Como se observa en la imagen, es necesario habilitar el reenvío, clicando el box **Streaming Enabled** e indicar la dirección IP de la PC destino en la sección **Server**.

### Filtrado

Como se muestra en la siguiente figura, se puede filtrar el contenido del tráfico capturado según interfaces, direcciones MAC, protocolos en capa 2, direcciones IP, protocolos en capa 3 y puertos, procesamiento según CPU, sentido del flujo de datos:

![](imagenes/SnifferFilter.png)
