# Source NAT


En los siguientes pasos, se muestran opciones para enmascarar la dirección IP de origen de los paquetes salientes de un router en función a una dirección IP disponible en la interfaz por la que sale el flujo de datos:

![](NAT.png)

## Acciones

### masquerade (enmascaramiento)

El funcionamiento de la acción **masquerade** permite ocultar el tráfico que cumpla con las reglas de filtrado a partir de una dirección IP de la interfaz del enrutador por la que salga el paquete.

```bash
/ip firewall nat add chain=srcnat action=masquerade
```

A diferencia del caso anterior, al indicar "out-interface", en el siguiente modo de configuración sólo se enmascaran los paquetes que salen por dicha interfaz:

```bash
/ip firewall nat add chain=srcnat action=masquerade out-interface="x"
```

Donde: |
-- | 
 **x** = interface de salida

Ejemplo: trasladar las direcciones IP de origen de los paquetes que salgan por la interfaz ether1 a una dirección IP de dicha interfaz:

```bash
/ip firewall nat add chain=srcnat action=masquerade out-interface=ether1
```