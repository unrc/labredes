# NAT - Network Address Translation

## Introducción

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQwXYufu5IvPUHSagb-viY2XsBSH3R7pTRd-K7qRjqVZpQysjsF0Ep7xFW1FLulW8I2vDID5lHHkfak/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="540" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

La relación entre direcciones IP y puertos, permite identificar y diferenciar múltiples flujos o servicios.

![](Flujos.png)
Figura 1. Esquema práctico con múltiples flujos

Considerando la figura 1, se pueden observar los siguientes servicios, asociados a puertos bien conocidos:

Servicio | Puerto
-- | --
**ssh** | 22
**telnet** | 23
**www** | 80

Por otra parte, se presenta un chat, programado por Catalina, que utiliza el puerto 50000 y que ambos usuarios acordaron.

De esta forma, considerando los servicios disponibles y las ejecuciones entre ambas computadoras, se pueden encontrar los siguientes flujos de datos:

Ejecución | Socket
-- | --
Roberto se conecta por ssh a la PC de Catalina | PCA:53245 <-> PCB:22
Catalina se conecta por ssh a la PC de Roberto | PCB:63572 <-> PCA:22
Catalina accede al sitio web de Roberto | PCB:49875 <-> PCA:80
Chat entre Roberto y Catalina | PCA:50000 <-> PCB:50000

Vease que si bien hay dos conexiones ssh en simultáneo, se pueden diferenciar a partir de las relaciones entre IPs y Puertos.
Notese a Catalina le será imposible conectarse por telnet al equipo de Roberto, ya que el servicio de telnet no se encuentra habilitado en la PC y por ende, no se encontrará esperando peticiones en el puerto 23. 

Existen múltiples situaciones, en las cuales es necesario modificar los campos de dirección IP o puertos. Para ello existe NAT (Network Address Translation), una implementación que modifica dichos valores a aquellos paquetes que cumplan con ciertas características en particular, las cuales se utilizan como filtrado.

* **source NAT* o **srcnat**: This type of NAT is performed on packets that are originated from a natted network. A NAT router replaces the private source address of an IP packet with a new public IP address as it travels through the router. A reverse operation is applied to the reply packets traveling in the other direction.

* **destination NAT** o **dstnat**: This type of NAT is performed on packets that are destined for the natted network. It is most commonly used to make hosts on a private network to be accessible from the Internet. A NAT router performing dstnat replaces the destination IP address of an IP packet as it travels through the router towards a private network.

Since RouterOS v7 the firewall NAT has two new INPUT and OUTPUT chains which are traversed for packets delivered to and sent from applications running on the local machine:

* **input**: - used to process packets entering the router through one of the interfaces with the destination IP address which is one of the router's addresses. Packets passing through the router are not processed against the rules of the input chain.
* **output**: - used to process packets that originated from the router and leave it through one of the interfaces. Packets passing through the router are not processed against the rules of the output chain.

## Visualización de tabla NAT
```
/ip firewall connection print
Flags: E - expected, S - seen-reply, A - assured, C - confirmed, D - dying,
F - fasttrack, s - srcnat, d - dstnat
 #     PR..          SRC-ADDRESS     DST-ADDRESS      TCP-STATE     
"#"    "protocolo"   "x.x.x.x:po"    "y.y.y.y":"pd"
```

Donde: | | |
-- | -- | --
**#** = número de línea | **x.x.x.x** = dirección IP de origen | **po** = puerto de origen
**protocolo** = protocolo | **y.y.y.y** = dirección IP de destino | **pd** = puerto destino


