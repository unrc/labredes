#Servicio OVPN

## Configuración de servidor OVPN

### Creación de certificados

Los pasos que se listan a continuación son una simplificación de los comandos que se deben implementar para la generación de certificados. Tener presente que pueden modificarse los nombres dentro de cada paso.

```bash
/certificate add name=CA common-name=CA key-usage=key-cert-sign,crl-sign days-valid=7300
/certificate sign CA ca-crl-host="IPserver" name=CA
/certificate add name=OVPNserver common-name=server days-valid=7300
/certificate add name=OVPNclient common-name=client days-valid=7300
/certificate sign OVPNserver ca=CA name=server
/certificate sign OVPNclient ca=CA name=client
/certificate export-certificate CA
/certificate export-certificate export-passphrase="Contraseña" client
```

Donde: | |
-- | -- |
**IPserver**= Dirección IP del servidor | **Contraseña**= contraseña asociada al certificado |

### Carga de perfil

```bash
/ppp profile add name="NombreDelPerfil" local-address="IPserver" remote-address="Pool" bridge="Bridge" use-mpls=default use-compression=default use-encryption=default only-one=yes  change-tcp-mss=default use-upnp=default address-list="" on-up="" on-down="" 
```

Donde: | |
-- | -- |
**NombreDelPerfil** = Nombre asociado al perfil | **IPserver** = Dirección IP del servidor |
**Bridge** = Bridge al que se le da acceso | **remote-address** = Puede generarse un pool con direcciones ip asociadas a la red que se quiere dar acceso. |

### Carga de clientes

```bash
/ppp secret add name="NombreDeCliente"  password="Contraseña" profile="NombreDelPerfil" service=ovpn remote-address="IPcliente"
```

Donde: | |
-- | -- |
**NombreDeCliente**= Nombre asociado al cliente | **Contraseña**=  Contraseña asociado al cliente |
**NombreDelPerfil**= Nombre del perfil asociado a OVPN| IPcliente**= Dirección IP asociada al cliente (si es necesario ponerla en forma estática, si no, indicar un pool en el profile) |

### Habilitación de servicio 

```bash
/interface ovpn-server server set enabled=yes port=1194 mode=ip netmask=16 default-profile="NombreDelPerfil" certificate="NombreCertificado" auth=sha1 cipher=aes256 require-client-certificate=yes set netmask=16
```

Donde: | |
-- | -- |
**NombreDelPerfil**= Nombre asociado al perfil | **NombreCertificado**= Nombre del certificado creado en el servidor (en estos pasos se cargó como **server**) |

## Configuración de cliente OVPN

### Archivo de configuración - cliente OVPN

Descargar [archivo](OVPN.ovpn)

```bash
client
dev tun
proto tcp
remote "IPservidor" 1194
resolv-retry infinite
nobind
persist-key
persist-tun
<ca>
-----BEGIN CERTIFICATE-----

INSERTAR CERTIFICADO DE AUTORIDAD

-----END CERTIFICATE-----
</ca>
<cert>
-----BEGIN CERTIFICATE-----

INSERTAR CERTIFICADO DE CLIENTE

-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN ENCRYPTED PRIVATE KEY-----

INSERTAR CLAVE PRIVADA

-----END ENCRYPTED PRIVATE KEY-----
</key>
remote-cert-tls server
cipher AES-256-CBC
auth SHA1
auth-user-pass
verb 3
```

Donde: |
-- |
**IPservidor**= Dirección IP desde donde se alcanza el servidor ovpn |

### Configuración de cliente ovpn en Linux

Para comenzar, es necesaria la instalación del software "network-manager-openvpn-gnome" en el equipo del cliente:

``` bash
sudo apt-get install network-manager-openvpn-gnome
```

