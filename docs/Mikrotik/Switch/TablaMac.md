#Virualización de tabla MAC

Mikrotik presenta su tabla MAC en la siguiente sección: **/interface bridge host**. Para conocer su contenido, se implementa la opción **print** de la forma:

```mikrotik
/interface bridge host print

Flags: D - DYNAMIC; L - LOCAL
Columns: MAC-ADDRESS, VID, ON-INTERFACE, BRIDGE
#     MAC-ADDRESS        VID  ON-INTERFACE  BRIDGE
"#l"  "MAC-Dispositivo"  "#v" "Interfaz"    "NombreBridge"
```

Donde: | |
-- | -- |
**#l**= Número de línea en la tabla | **MAC-Dispositivo**= Dirección MAC de PC/Dispositivo conectado |
**#v**= Número de vlan | **Interfaz**= Nombre de interfaz en Mikrotik |
**NombreBridge**= Nombre de bridge| |

Tener presente que aquellas líneas que presenten una bandera **L**, representan interfaces propias del equipo.