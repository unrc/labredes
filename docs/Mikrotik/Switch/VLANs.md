# Asignación de puertos a VLANs en Bridges

## Creación de un bridge

A diferencia de cómo se presenta la creación de un bridge en [enlace](../Bridge), para la implementación de vlans en un bridge, es necesario habilitar el argumento **vlan-filtering**:

```mikrotik
/interface bridge add ingress-filtering=no name="NombreBridge" vlan-filtering="Filtro"
```

Donde: | |
-- | -- |
**NombreBridge** = nombre del bridge a configurar | **Filtro** = [**yes**/**no**] habilita el funcionamiento de etiquetado de VLANs. |

## Configuración de puerto en modo acceso
Una vez creado un bridge con la opción **vlan-filtering** habilitada, se puede configurar un puerto en modo acceso indicando el número de vlan en el argumento **pvid** de la forma:

```mikrotik
/interface bridge port add bridge="NombreBridge" interface="Interfaz" pvid="#v"
```

Donde: | |
-- | -- |
**NombreBridge** = nombre del bridge a configurar | **Interfaz** = Nombre de los puertos que se adicionan al Bridge. | 
**#v** = número de vlan ||

## Configuración de puertos en modo troncal

Para la asignación de uno o más puertos en modo troncal, se debe acceder a la sección **/interface/bridge/vlan** y configurar lo siguiente: 

```mikrotik
/interface bridge vlan add bridge="NombreBridge" tagged="Interfaces" untagged="interfaces" vlan-ids="#v"
```

Donde: | |
-- | -- |
**NombreBridge** = nombre del bridge a configurar | **Interfaces**  = nombre de la/s interfaz/ces que se asignan. Si es más de una, se separan por "," (comas). |
**#v** = número de vlan ||

Durante la asignación de puertos a VLANs, es necesario contemplar que si hay puertos troncales y de acceso en un mismo Bridge, puede ser necesario ingresar el nombre del Bidge como una interfaz donde se etiquetan las vlans en cuestión.

## Visualización de asignación de vlans

```mikrotik
/interface/bridge/vlan/print
Flags: D - DYNAMIC
Columns: BRIDGE, VLAN-IDS, CURRENT-TAGGED, CURRENT-UNTAGGED
#   BRIDGE          VLAN-IDS   CURRENT-TAGGED  CURRENT-UNTAGGED
0 D "NombreBridge"      "#v"       "Interfaz"        "Interfaz"
```

Donde: | |
-- | -- |
**NombreBridge** = nombre del bridge a configurar | **#v** = número de vlan |
**Interfaz**  = nombre de la/s interfaz/ces asociadas a cada vlan.||