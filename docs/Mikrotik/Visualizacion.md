# Visualización de configuración

## Visualización parcial

Mediante el comando **print** se puede visualizar la configuración dentro de una sección en particular. Para ello es necesario ingresar a la rama en particular correspondiente a la configuración. Ejemplos:

Conocer las direcciones ip:

```bash
/ip address print
```

Visualizar tabla de rutas: 

```bash
/ip route print
```

## Visualización completa

El comando **export**, implementado en la raíz de los comandos, presenta toda la configuración en forma de comandos (salvo información crítica, como contraseñas de usuarios).

### Descargar configuración en un archivo

A partir de **export**, se puede adicionar el argumento **file=** seguido de un nombre, lo que generará un archivo con la configuración, el cual puede descargarse posteriormente.