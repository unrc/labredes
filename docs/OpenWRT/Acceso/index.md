# Opciones de acceso y configuración
## Conexión

Para acceder a la configuración de un equipo OpenWrt, es necesario tener una interconexión a una de sus interfaces LAN:

![](ConexionRed.png)


## Acceso web

Para acceder al entorno web, ingresar como url a la dirección IP del enrutador: 192.168.1.1

![](AccesoWeb.png)

## SSH

Para el acceso por ssh puede ser necesario que previamente se cargue una contraseña, por lo que es recomendable que el primer uso sea por acceso web.

El acceso ssh se realiza con un cliente ssh, donde en un terminal se ejecute:

```
PC# ssh root@192.168.1.1
```

Para más información sobre ssh en linux: [enlace](../../Linux/ssh/)