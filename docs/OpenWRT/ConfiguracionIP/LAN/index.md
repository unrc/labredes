# Interfaz LAN

Esta interfaz representa la red interna del enrutador. 

### Configuración IP

![](LanIP.png)

Figura 1. Parámetros IP para la interfaz LAN

En la imagen 1 se observan los siguientes parámetros:

* Dirección IP: 192.168.1.1

* Máscara de red: 255.255.255.0


### Servidor DHCP

La interfaz LAN cuenta con un servidor DHCP, el cual brinda configuración en forma automática al conjunto de dispositivos que se conecte a cualquiera de sus puertos:

![](LanDHCPServer.png)

Figura 2. Servidor DHCP

En la imagen 2 se muestra que el servidor DHCP comienza a partir del número 100 (en el último octeto) y tiene capacidad para 150 direcciones IP.