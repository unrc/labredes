# Interfaz WAN

## Cliente DHCP

![](WANDHCP.png)

Figura 1. Configuración de WAN como Cliente DHCP

## Configuración Estática

![](WANStaticIP.png)

Figura 2. Configuración de parámetros IP en forma estática

## Cliente PPPoE 

![](WANPPPoE.png)

Figura 3. Configuración de WAN como Cliente PPPoE