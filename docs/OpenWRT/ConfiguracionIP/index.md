# Configuración IP

Para acceder a los parámetros de configuración IP de cada interfaz, en el menú superior se debe seleccionar: 
**Network > Interfaces**, como se muestra en la siguiente figura:

![](NetworkInterfaces.png)

Figura 1. Acceso a configuración de interfaces

Considerando los esquemas presentes en [enlace](../EstructuraLogica/), se puede acceder a la configuración de dos tipos de interfaces lógicas **LAN** y **WAN**:

![](Interfaces.png)

Figura 2. Interfaces disponibles