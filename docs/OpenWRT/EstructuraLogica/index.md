#Estructura Lógica

## OpenWRT Virtualizado

![](./EstructuraLogicaVirtual.png)

Figura 1. Representación lógica de equipo virtualizado 

Como se observa en la imagen anterior, un dispositivo virtualizado cuenta con las interfaces: 

| Nombre de interfaz | Configuración por defecto | Red lógica a la que pertenece |
| -- | -- | -- |
| eth1 | Cliente DHCP | wan |
| eth0 | Sin configuración | lan |
| br-lan | 192.168.1.1/24 | lan |

## OpenWRT sobre equipo TP-LINK o equivalente

| Ejemplo de equipo TP-Link 740n | |
| -- | -- |
| ![](./Tp-Link-740n-frente.png)| ![](./Tp-Link-740n-atras.png)|
| Figura 2. Frente de equipo TP-Link 740n | Figura 3. Dorso de equipo TP-Link 740n |

En relación a la imagen 3, le figura 4 representa la asociación de los puertos azules y amarillos de la forma: 

![](./EstructuraLogicaFisica.png)

Figura 4. Dorso de equipo TP-Link 740n

Donde:

* el puerto azul es la interfaz eth1, donde se debe hacer la conexión wan.

* los cuatro puertos amarillos y la interfaz inalámbrica estan interconectados entre sí a la eth0. A su vez, estas bocas se encuentran asociadas a la IP: 192.168.1.1/24, desde donde se ofrece un servicio de DHCP