# Elementos

Algunos dispositivos disponibles son los siguientes:

![](imagenes/elementos.jpg)

Seleccionar los elementos necesarios para la implementación y desplazarlos hacia el área de trabajo.

## Cables

![](imagenes/cables.jpg)

### Estados de enlaces

Una vez interconectados los dispositivos, observar los indicadores de estado:

![](imagenes/estado_enlaces.jpg)
