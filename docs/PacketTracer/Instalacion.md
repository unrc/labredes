# Instalación de Packet Tracer

Para la descarga e instalación de Packet Tracer es necesario registrarse en Netacad (sitio web académico de Cisco)

## Descarga del instalador

### Acceso a netacad

Acceder a ​[​www.netacad.com](https://www.netacad.com/es) y allí seleccionar la opción "iniciar sesión" que se muestra en el margen superior derecho de la página:

![](acceso.jpg)

### Inicio de sesión

Ingresar el nombre de usuario o correo electrónico de la cuenta de Cisco y luego la contraseña:

![](inicio_sesion.jpg)

En caso de no poseer, crear una cuenta.

### Descarga del programa

Una vez dentro del portal, seleccionar en el menú superior la sección "Recursos", y luego la opción "Descargar Packet Tracer":

![](menu_descarga.jpg)

Alli desplazarse hasta la parte inferior donde se puede descargar el instalador en función al sistema operativo en el cual se quiere instalar:

![](seleccionar.jpg)

## Windows

Packet Tracer 8 tiene compatibilidad para Windows 8, Windows 10 y Windows 11.

Para la instalación, descargar el programa y ejecutarlo. Al iniciar el programa es necesario cargar nombre de usuario y contraseña.


## Ubuntu 20.04

En esta versión de Linux es posible instalar Packet Tracer 8.

Desplazarse hasta la carpeta donde se encuentra el instalador y luejo ejecutarlo de la forma:

```
sudo apt update
sudo apt upgrade
sudo apt install "NombreDelInstalador"
```
