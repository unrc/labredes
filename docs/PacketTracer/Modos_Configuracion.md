#Modos de Configuración

Al hacer doble click sobre cada dispositivo, se puede entrar a sus opciones de configuración:

## Modo físico

Se puede conocer aspectos que hacen al dispositivo y se pueden modificar algunas de las placas e interfaces disponibles.

![](imagenes/Modo_Fisico.jpg)

## Modo gráfico

Configuración básica de algunos parámetros de red.

![](imagenes/Modo_Grafico.jpg)

## Modo Consola - CLI (Command Line Interface)

Configuración del equipo por consola, equivalente a la conexión serial.

![](imagenes/Modo_Consola.jpg)

## Modo Escritorio (disponible en PC)

Opción de configuración equivalente al presentado por un sistema operativo con entorno gráfico, con opciones para hacer pruebas.

![](imagenes/Modo_Escritorio.jpg)
