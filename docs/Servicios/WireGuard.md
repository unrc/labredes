#WireGuard

##Servidor Mikrotik - Cliente Xubuntu

1) Ingresar en el equipo Mikrotik y generar una interfaz  WireGuard:

```bash
/interface wireguard add listen-port="Puerto" mtu=1420 name="InterfazWireGuard"
```

Donde:| |
-- | --|
**Puerto** = Puerto a utilizar para la conexión WireGuard (Por defecto: 13231) | **InterfazWireGuard** = Nombre asignado a la interfaz lógica asociada a WireGuard |  

2) Asignar una dirección de IP a la interfaz WireGuard:

```bash
/ip address add address="IPWireGuardLMKT" interface="InterfazWireGuard"
```

Donde:| |
-- | --|
**IPWireGuardLMKT**= IP para interfaz WireGuard | **InterfazWireGuard** = Nombre asignado a la interfaz lógica asociada a WireGuard |  

3) Instalar WireGuard en Linux:

```bash
sudo apt update
sudo apt install wireguard
```

Generar el par de claves pública y privada con el comando:

```bash
wg genkey | tee private.key | wg pubkey > public.key
```

4) Crear un archivo de configuración:

```bash
sudo nano /etc/wireguard/wg.conf
```

5) En el archivo de configuración, cargar el contenido:

```bash
[Interface]
PrivateKey = "ClavePrivadaLinux"
Address = "IPWireGuardLinux"
ListenPort = "Puerto"

[Peer]
PublicKey = "ClavePublicaMKT"
AllowedIPs = "IPRedInternaMKT"
Endpoint = "IPMKT":"Puerto"
```

Donde:||
-- | -- |
**ClavePrivadaLinux** = Clave generada en Linux. (cat private.key) | **IPWireGuardLinux** = Dirección IP de la interfaz WireGuard en Linux |
**Puerto** = Puerto a utilizar para la conexión WireGuard (Por defecto: 13231) | **ClavePublicaMKT** = Clave pública generada en el Mikrotik | 
**IPMKT** = Dirección IP del equipo Mikrotik |

Para obtener la clave privada en el dispositivo Linux:

```bash
cat private.key
```

Para obtener la clave pública en el dispositivo Mikrotik:

```bash
/interface/wireguard/print 
Flags: X - disabled; R - running 
 0  R name="InterfazWireGuard" mtu=1420 listen-port="Puerto" private-key="ClavePrivadaServidor" public-key="ClavePublicaServidor"
```

Donde:||
-- | -- |
**InterfazWireGuard** = Nombre asignado a la interfaz lógica asociada a WireGuard | **Puerto** = Puerto donde espera coneccion WireGuard |
**ClavePrivadaServidor** = Clave privada | **ClavePublicaServidor** = Clave pública | 


6) Cargar el peer correspondiente al cliente Linux

```bash
/interface/wireguard/peers/add interface"InterfazWireGuard" public-key="ClavePublicaLinux" allowed-address="IPRedInternaMKT" endpoint-port="Puerto"
```

Donde:||
-- | -- |
**InterfazWireGuard** = Nombre asignado a la interfaz lógica asociada a WireGuard | **ClavePrublicaLinux** = Clave generada en Linux. (cat public.key) | **IPRedInternaMKT** = Dirección IP que se permite alcanzar por WireGuard | **Puerto** = Puerto a utilizar para la conexión WireGuard (Por defecto: 13231) |

Para obtener la clave pública del dispositivo Linux:

```bash
cat public.key
```

7) Iniciar la conexión en Linux, ejecutar el comando:

```bash
sudo wg-quick up wg
```
