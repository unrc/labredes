# Exportar una máquina virtual

Es posible exportar una máquina virtual, contemplando no sólo sus discos rígidos virtuales si no también la configuración completa.

Para ello, acceder al menú "File > Export Appliance", seleccionar la máquina virtual a exportar y generar un archivo con extención "ova":

![](imagenes/Export.jpg)
