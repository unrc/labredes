# Instalación de VirtualBox

## Instalación desde el sitio web

Descarga de archivo de instalación: [link](https://www.virtualbox.org/wiki/Downloads) según el sistema operativo que corresponda y luego ejecutarlo.

El el caso de linux, utilizar el "Gestor de Software" o acceder a la carpeta donde se realizó la descarga y mediante el comando:

```
sudo apt install ./virtualbox*
```

## Instalación desde repositorio de Linux

VirtualBox se puede instalar directamente por consola desde el repositorio de linux, pero puede que la versión no sea la más actualizada:

```
sudo apt update
sudo apt install virtualbox
```
