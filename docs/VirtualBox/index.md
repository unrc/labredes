#VirtualBox

VirtualBox es un software de virtualización para virtualizar computadoras con arquitectura de 32 y 64 bits. Es un Open Source Software bajo los términos GNU General Public License (GPL) version 2. 
Funciona en computadoras con Windows, Linux, Macintosh y Solaris. [Sitio oficial](https://www.virtualbox.org/)

Ofrece los siguientes escenarios:

* Ejecución de múltiples sistemas operativos en una misma PC.
* Simple instalación de software: Puede utilizarse Máquinas Virtuales para compartir implementaciones completas (Appliances). 
* Optimización de la infraestructura: Puede reducir costos de hardware y energía.
* Evaluación y recuperación ante desastres: Una máquina virtual puede considerarse como un contenedor, que puede ser congelado, copiado, transportado... Cualquier estado en una máquina virtual puede ser guardado y guardado como un backup (Snapshots).
* Portabilidad.
* Soporte entre el hardware de la PC y la máquina virtual
* Una arquitectura limpia.
* Modularidad.
