



## netstat


netstat -e -s

## tracert

```
tracert "Destino"
```

Donde: |
 :-: |
**Destino** = Dirección IP o Dominio a donde se quiere conocer la traza |