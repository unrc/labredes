#Tutoriales disponibles

| Equipamiento de red | | | | |
| :-: | :-: | :-: | :-: | :-: |
|<a href="./Cisco/"><img src="https://www.cisco.com/web/fw/i/logo-open-graph.gif" alt="drawing" width="80"/></a>|<a href="./Linux/"><img src="https://upload.wikimedia.org/wikipedia/commons/3/35/Tux.svg" alt="drawing" width="80"/></a>|<a href="./Mikrotik/"><img src="./Mikrotik/imagenes/mkt.png" alt="drawing" width="80"/></a>|<a href="./Huawei/"><img src="./Huawei/huawei-logo.png" alt="drawing" width="80"/></a>|<a href="./OpenWRT/"><img src="https://avatars.githubusercontent.com/u/5120842" alt="drawing" width="80"/></a>|
| [Cisco](./Cisco/) | [Linux](./Linux/) | [Mikrotik](./Mikrotik/) | [Huawei](./Huawei/) | [OpenWRT](./OpenWRT)

| Contenedores | Virtualización | Entorno de virtualización | Simulación |
| :-: | :-: | :-: | :-: |
|<a href="./Docker/"><img src="./Docker/Docker.png" alt="drawing" width="80"/></a>|<a href="./VirtualBox/"><img src="https://upload.wikimedia.org/wikipedia/commons/d/d5/Virtualbox_logo.png" alt="drawing" width="80"/></a>|<a href="GNS3/"><img src="./GNS3/gns3.png" alt="drawing" width="80"/></a>|<a href="./PacketTracer/"><img src="./PacketTracer/PacketTracer.png" alt="drawing" width="80"/></a>|
| [Docker](./Docker/) | [Virtual Box](./VirtualBox/) | [GNS3](./GNS3/) | [PacketTracer](./PacketTracer/)|

| Instalador de Programas | Diagramas de Red | 
| :-: | :-: | 
| <a href="./scripts/"><img src="imagenes/install.png" alt="drawing" width="120"/></a>|<a href="Diagrams/"><img src="https://upload.wikimedia.org/wikipedia/commons/3/3e/Diagrams.net_Logo.svg" alt="drawing" width="80"/></a>|
| [Scripts instaladores](./scripts/) | [diagrams.net](Diagrams/) | 

| Laboratorios | 
| :-: | 
| <a href="./Labs/"><img src="imagenes/lab.jpg" alt="drawing" width="80"/></a> |
| [Labs](./Labs/) |
