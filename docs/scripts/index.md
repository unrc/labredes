# Scripts instaladores

El siguiente es un script que permite la instalación de gran parte de los programas necesarios para las asignaturas en la orientación de redes de datos.

El código incluye los siguientes programas:

* Actualizacion OS
* Brave 
* [Docker](../../docker/Docker_Instalacion/)
* geany
* [gns3](../../gns3/Instalacion/)
* iperf3
* [ssh](../../linux/SSH/)
* [Virtualbox](../../virtualbox/Instalacion/)
* vlc
* [Winbox](../../mikrotik/Winbox/) (incluye la instalación de wine)
* Wine 
* [Wireshark](../../linux/Captura_de_trafico/Wireshark/instalacion/)


## Uso del instalador

1) Descargar el script del instalador desde el siguiente [enlace](instalador.sh).

2) Dentro de la carpeta donde se encuentra el script, cambiar los permisos para que sea ejecutable:

```bash
cd "ubicacion"
chmod +x "instalador.sh"
```

Donde: |
-- |
**ubicación** = directorio donde se encuentra el script | 

3) Ejecutar el archivo como usuario convensional, NO COMO SUPERUSUARIO y seleccionar la/s opción/es a instalar:

```bash
./"instalador.sh"
```

Donde: |
-- |
**ubicación** = directorio donde se encuentra el script |

