#!/bin/bash

#export NEWT_COLORS='
#window=,#33afff
#border=white,#33afff
#textbox=white,#33afff
#button=black,white
#'

CHOICES=$(whiptail --title "Instalador" --checklist "Seleccione los programas que desea instalar:" 20 60 12 \
brave "Navegador web" OFF \
docker "Gestor de contenedores" OFF \
geany "Editor de textos" OFF \
gns3 "Virtualización de redes" OFF \
iperf3 "Generador de tráfico UDP y TCP" OFF \
putty "Conexión por consola" OFF \
ssh "Servidor y cliente ssh" OFF \
virtualbox "Hipervisor de máquinas virtuales" OFF \
vlc "Reproductor multimedia" OFF \
vnc-server "Servidor para acceso remoto" OFF \
winbox "Gestor de Mikrotik (instala wine)" OFF \
wine "Soporte para software windows" OFF \
wireshark "Captura y análisis de tráfico" OFF \
python "Programa + librerías" OFF \
3>&1 1>&2 2>&3)

if [ -z "$CHOICE" ]; then
	for CHOICE in $CHOICES; do
		case "$CHOICE" in
			"\"brave\"")
				sudo apt install apt-transport-https curl -y
				sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
				echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
				sudo apt update
				sudo apt install brave-browser -y
				clear
				;;	
			"\"docker\"")
				sudo apt remove docker docker-engine docker.io containerd runc
				sudo apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common -y
				curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
				sudo add-apt-repository \
				   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
				   $(lsb_release -cs) \
				   stable"
				sudo apt install -y docker-ce docker-ce-cli containerd.io
				sudo groupadd docker
				sudo usermod -aG docker $USER
				sudo chmod 666 /var/run/docker.sock
				apt install docker-compose -y
				clear
				echo "Para que docker funcione correctamente, es necesario reiniciar la PC"
				;; 
			"\"geany\"")
				sudo apt install -y geany
				clear
				;;
			"\"gns3\"")
				sudo add-apt-repository ppa:gns3/ppa -y
				sudo apt install -y gnome-terminal tigervnc-standalone-server tigervnc-viewer gns3-gui gns3-server 
				# Correccion de errores:
				sudo chown "user":"user" /dev/kvm
				sudo chmod +x /usr/bin/ubridge
				sudo chmod +x /usr/bin/dumpcap
				cp /usr/share/applications/gns3.desktop $(xdg-user-dir DESKTOP)/
				chmod +x $(xdg-user-dir DESKTOP)/gns3.desktop
				clear
				;;			
			"\"iperf3\"")
				sudo apt remove iperf3 -y
				sudo apt autoremove -y
				sudo apt install git build-essential lib32z1 -y
				git clone https://github.com/esnet/iperf.git
				cd iperf
				sudo ./configure
				sudo make
				sudo make install
				cd ..
				sudo rm -R iperf
				clear
				;;
			"\"putty\"")
				sudo apt install -y putty
				sudo apt remove brltty
				cp /usr/share/applications/putty.desktop $(xdg-user-dir DESKTOP)/
				chmod +x $(xdg-user-dir DESKTOP)/putty.desktop
				clear
				;;
			"\"ssh\"")
				sudo apt install -y openssh-server
				clear
				;; 
			"\"virtualbox\"")
				sudo apt install -y virtualbox virtualbox-ext-pack
				cp /usr/share/applications/virtualbox.desktop $(xdg-user-dir DESKTOP)/
				chmod +x $(xdg-user-dir DESKTOP)/virtualbox.desktop
				clear
				;; 
			"\"vlc\"") 
				sudo apt install -y vlc
				clear
				;; 
			"\"vnc-server\"")
				sudo apt-get install x11vnc
				wget https://unrc.gitlab.io/labredes/scripts/x11vnc.service
				sudo mv x11vnc.service /lib/systemd/system/x11vnc.service
				sudo systemctl daemon-reload
				sudo systemctl enable x11vnc.service
				sudo systemctl start x11vnc.service
				clear
				;; 
			"\"winbox\"")
				sudo apt install -y wine64
				wget https://mt.lv/winbox64
				mv winbox64 $(xdg-user-dir DESKTOP)/
				wine64 $(xdg-user-dir DESKTOP)/winbox64 &
				clear
				echo "Se descargó winbox64.exe en esta carpeta e instaló wine64"
				;; 
			"\"wine\"")
				sudo apt install -y wine64
				clear
				;; 
			"\"wireshark\"")
				sudo apt install -y wireshark
				cp /usr/share/applications/org.wireshark.Wireshark.desktop $(xdg-user-dir DESKTOP)/
				chmod +x $(xdg-user-dir DESKTOP)/org.wireshark.Wireshark.desktop
				clear
				;;
			"\"python\"")
				rm instalador_python.sh
				wget instalador https://unrc.gitlab.io/labredes/scripts/instalador_python.sh
				chmod +x instalador_python.sh
				./instalador_python.sh
				rm instalador_python.sh
				clear
				;;
    *)
      echo "Opción no soportada!" >&2
      exit 1
      ;;
    esac
  done
else
  echo "No se selecteccionó alguna opción"
fi
rm instalador_tmp.sh
